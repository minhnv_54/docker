### Mục lục

- [I. Overview về docker container](#I)
	* [1. Container và VM](#1.1)
	* [2. Giới thiệu về Docker ](#1.2)
	* [3. Cài đặt Docker ](#1.3)
	* [4. Cấu hình proxy ](#1.4)
	* [5. Hello word ](#1.5)
- [II. Các câu lệnh cơ bản](#II)
	* [2.1. Build and run,tag](#2.1)
	* [2.2. Push, pull](#2.2)
- [III. Kiến thức thêm](#III)
	* [3.1. Docker CE/Docker EE](#3.1)
	* [3.2. Docker Engine](#3.2)
	* [3.3. Docker Compose](#3.3)
	* [3.4. Docker instruction](#3.4)
	* [3.5. Docker volume and bind mounts](#3.5)
	* [3.6. Docker network](#3.6)
- [IV. Issue: Link and net host in docker ](#IV)  

<a name="I"></a>
**I. Overview về docker container**

  
<a name="1.1"></a>
**1. Container và VM**

**a)VM**

![image](/uploads/6d70ac6dab8700f8383cc0d2edc5d419/image.png)


-Vms về bản chất là một giả lập của một máy tính để thực thi các ứng dụng giống như một máy tính thật, cung cấp sự ảo hóa về phần cứng. Host machine sẽ cung cấp cho VMs những tài nguyên như là RAM, CPU. Những tài nguyên đó sẽ được phân bổ giữa các VMs theo cách mà bạn cho là hợp lý.

-Những VMs chạy trên host machine thường được gọi là guest machine. Guest machine này sẽ chứa tất cả những thứ mà hệ thống cần để chạy ứng dụng như hệ điều hành (OS), system binaries và libraries. VMs chạy trên hệ điều hành của host machine và không thể truy cập trực tiếp đến phần cứng mà phải thông qua hệ điều hành.

 -*Ưu:*  

- Độc lập với system’s kernel, do đó sẽ không bị xung đột khi sử dụng.


-*Nhược:*

 
-Về tài nguyên: Khi bạn chạy máy ảo, bạn phải cung cấp "cứng" dung lượng ổ cứng cũng như ram cho máy ảo đó, bật máy ảo lên để đó không làm gì thì máy thật cũng phải phân phát tài nguyên.

-Về thời gian: Việc khởi động, shutdown khá lâu, có thể lên tới hàng phút.


**b) Container**

![image](/uploads/5c1550f06b16adce9121ddf98d70a4e6/image.png)

-Container không giống như VMs, Container không cung cấp sự ảo hóa về phần cứng. Một Container cung cấp ảo hóa ở cấp hệ điều hành bằng một khái niệm trừu tượng là “user space”. Sự khác nhau lớn nhất của Container và VMs là Container có thể chia sẻ host system’s kernel với các container khác

*-Ưu điểm:*

-   Linh động: Triển khai ở bất kỳ nơi đâu do sự phụ thuộc của ứng dụng vào tầng OS cũng như cơ sở hạ tầng được loại bỏ.
    
-   Nhanh: Do chia sẻ host OS nên container có thể được tạo gần như một cách tức thì.
    
-   Nhẹ: Container cũng sử dụng chung các images nên cũng không tốn nhiều disks.
    
-   Đồng nhất :Khi nhiều người cùng phát triển trong cùng một dự án sẽ không bị sự sai khác về mặt môi trường.
    
-   Đóng gói: Có thể ẩn môi trường bao gồm cả app vào trong một gói được gọi là container. Có thể test được các container. Việc bỏ hay tạo lại container rất dễ dàng.

*-Nhược:* 

- Vì dùng chung kernel nên nếu sử dụng hđh không tương thích sẽ không chạy được
- Không có tính độc lập như khi sử dụng VMs
- Tính an toàn: 

       + Do dùng chung OS nên nếu có lỗ hổng nào đấy ở kernel của host OS thì nó sẽ ảnh hưởng tới toàn bộ container có trong host OS đó.

       + Nếu host OS là Linux, nếu ai đó trong 1 container chiếm quyền điều khiển, tầng OS sẽ bị tấn công cùng các container khác trong hệ thống.

 
<a name="1.2"></a>
**2. Giới thiệu về Docker**
- Việc setup và deploy application lên một hoặc nhiều server rất vất vả từ việc phải cài đặt các công cụ, môi trường cần cho application đến việc chạy được ứng dụng chưa kể việc không đồng nhất giữa các môi trường trên nhiều server khác nhau. Chính vì vậy Dockẻ ra đời để giải quyết vấn đề này.


- Docker là một nền tảng cho developers và sysadmin để develop, deploy và run application với container. Nó cho phép tạo các môi trường độc lập và tách biệt để khởi chạy và phát triển ứng dụng và môi trường này được gọi là container. Khi cần deploy lên bất kỳ server nào chỉ cần run container của Docker thì application của bạn sẽ được khởi chạy ngay lập tức.

- Docker sử dụng kiến trúc client-server.  Docker server (hay còn gọi là daemon) sẽ chịu trách nhiệm build, run, distrubute Docker container. Docker client và Docker server có thể nằm trên cùng một server hoặc khác server. Chúng giao tiếp với nhau thông qua REST API dựa trên UNIX sockets hoặc network interface.

+ Docker daemon (dockerd) là thành phần core, lắng nghe API request và quản lý các Docker object. Docker daemon host này cũng có thể giao tiếp được với Docker daemon ở host khác.

+ Docker client (docker) là phương thức chính để người dùng thao tác với Docker. Khi người dùng gõ lệnh docker run imageABC tức là người dùng sử dụng CLI và gửi request đến dockerd thông qua api, và sau đó Docker daemon sẽ xử lý tiếp.

***Docker objects:**

-   **Images:** là 1 template chỉ đọc để tạo ra Docker container. Thường thì nó sẽ dựa trên 1 images khác, và thêm vào 1 số customization. Ví dụ như bạn có thể xây dựng 1 image dựa trên image ubuntu, sau đó install web server (như Apache) và application của bạn + 1 số thứ để app đó có thể chạy.
    
- Bạn có thể tự tạo image hoặc sử dụng image của người khác đã push lên registry or docker hub. Để tự tạo image, bạn viết 1 file tên là “Dockerfile” rồi build nó. Mỗi instruction trong Dockerfile là 1 lớp trong image, vì thế khi bạn thay đổi docker file thì phần nào thanh đổi thì lớp image tương ứng mới bị rebuid lại
    
-   **Container: ....**
    
-   **Service:** cho phép scale container qua nhiều Docker daemon, tất cả hoạt động cùng nhau như 1 swarm với multi managers và workers.
    
<a name="1.3"></a>
**3. Cài đặt docker**

`$sudo apt-get update`

`$sudo apt install docker.io`

`$sudo systemctl start docker`

`$sudo systemctl enable docker`

-Sử dụng Docker Command mà không cần quyền sudo:

`$ sudo addgroup --system docker`

`$ sudo adduser <username> docker`

`$ newgrp docker` 

<a name="1.4"></a>
**`4. Cấu hình proxy`**  

Khi bạn sử dụng Docker trong môi trường công ty, đôi khi bạn buộc phải sử dụng proxy HTTP vì các kết nối gửi đến cổng 80 và 443 thường bị chặn. Dưới đây hướng dẫn config proxy cho Docker

**`a) Cho docker daemon`**

![image](/uploads/f785006c45d45b4e0f7235e07aa00f86/image.png)

-**Docker daemon** :Docker daemon (dockerd) nghe các yêu cầu từ Docker API và quản lý các đối tượng Docker như =images, containers, network và volumn. Một daemon cũng có thể giao tiếp với các daemon khác để quản lý các Docker services.

Ví dụ:

-   Terminal gửi đoạn lệnh vào Docker CLI
    
-   Docker CLI sau khi nhận đoạn lệnh trên sẽ gửi nó đến Docker Server (hay còn gọi là Docker Daemon). Chúng ta không bao giờ tương tác trực tiếp với Docker Daemon; mọi thao tác đều sẽ phải thông qua Docker CLI
    
-   Docker Daemon, sau khi nhận yêu cầu từ Docker CLI, biết rằng chúng ta đang muốn khởi tạo một Docker Container từ Docker Image của PostgreSQL. Việc đầu tiên Docker Daemon làm đó là kiểm tra xem trong Image Cache đã tồn tại file Docker Image của PostgreSQL chưa. Trong trường hợp này, vì đây là lần đầu tiên chúng ta chạy câu lệnh Docker do đó trong Image Cache sẽ không có gì cả, khi đó Docker Daemon sẽ gửi lên Docker Hub một request yêu cầu download một Docker Image của PostgreSQL.
    
-   **Docker Image** là một file chứa tất cả các dependencies và cấu hình cần thiết để chạy một program.
    
-   Còn **Docker Hub** là một nơi lưu trữ tất cả các Docker Image mà bạn có thể download miễn phí
    
-   Sau khi Docker Image của PostgreSQL được lưu vào ổ cứng, Docker Daemon sẽ sử dụng Docker Image này để tạo ra một **Docker Container** chạy PostgreSQL. **Docker Container** này bản chất là một program được máy tính cấp cho một vùng nhớ riêng, một vùng ổ cứng riêng

![image](/uploads/cfaa5dcdddc0b1f925fe2b030990689b/image.png)
    

→ Mỗi program được khởi tạo từ Docker Image được gọi là Docker Container.

- Docker Container bản chất là một process (hoặc một nhóm các process) được kernel cấp phát cho một lượng resource nhất định để sử dụng. Mỗi khi Docker Container thực hiện 1 system call đến kernel, kernel sẽ điều hướng nó đến một phân vùng nhất định trên ổ cứng với các tài nguyên (RAM, CPU, network bandwidth) tương ứng.


**- Config proxy cho docker daemon**

```
# mkdir -p /etc/systemd/system/docker.service.d

# cat > /etc/systemd/system/docker.service.d/http-proxy.conf << EOF
[Service]
Environment="HTTP_PROXY=http://your.proxy:8080"
Environment="HTTPS_PROXY=http://your.proxy:8080"
Environment="NO_PROXY=127.0.0.1,localhost
EOF

# systemctl daemon-reload
# systemctl restart docker
```

**b) Cho Docker client**
- Tạo và sửa file ~/.docker/config.json :

![image](/uploads/9f1cee7c9d7e9a6478a4a654e9a15461/image.png)

- Restart:
```
# systemctl daemon-reload
# systemctl restart docker
```
> Khi nào cần config proxy cho Docker client, Docker Deamon:
- Docker deamon: Khi đứng ở máy chủ ra ngoài mạng 
- Docker client: Khi đứng từ container ra ngoài mạng

<a name="1.4"></a>
**5. Hello world **

![image](/uploads/aef6340c749b3b5a6eb320f3ee8f1c3d/image.png)

![image](/uploads/4e58de7c6b01e64e0a5273a48f483b39/image.png)

![image](/uploads/69bc7d38a16ff6d5e8992765c27833a0/image.png)


<a name="II"></a>
### II.Các câu lệnh cơ bản

<a name="2.1"></a>
**2.1.Build and run,tag**

ref : [https://docs.docker.com/get-started/part2/](https://docs.docker.com/get-started/part2/)

*** Tạo image:**

- Lệnh Build được sử dụng khi bạn muốn xây dựng 1 image, nó hoạt động bằng cách viết 1 Dockerfile, chứa các nội dung cần thiết cho 1 app, rồi từ đó build lên image

![image](/uploads/9c57423084b97208cd63f16bcde49305/image.png)
  

- -t : Docker cung cấp cách để tag image với 1 cái tên có thể chọn, format :'name:tag'

VD: borabanei:1.0 : borabanei là tên của image và 1.0 là tag.


- ". ": chỉ đường dẫn của dockerfile, “.” để chỉ dockerfile đang ở local

![image](/uploads/d06e9907875badf1dc19dfe1a23320d6/image.png)

- Download image từ docker hub :

   `sudo docker run centos`
    
***Start container:**

**+** From image :

![image](/uploads/77c7e7debafc4a9c75ad844dfddb0c09/image.png)
  

- --publish : chuyển traffic đến port 8000(trái) trên host và 8080(phải) trên container. Do các container của docker được gộp và chạy trong một network riêng nên nó sẽ độc lập với máy chủ mà docker chạy trên đó. Để mở các cổng của network các container và ánh xạ nó với cổng của máy host 
ta sử dụng tùy chọn --publish, -p. Hoặc sử dụng --publish-all, -P sẽ mở tất cả các cổng của container.


- --detach: run container trên bachground

- --name: gán cho container 1 cái tên (trường hơp này là bb)

-  borabanei:1.0 : name_image:tag_image

Access vào container đang chạy: 127.0.0.1:8000

![image](/uploads/297390aa0f245a8510e4ea78751a8f1c/image.png)
  
* 1 số lệnh check và optinal*

![image](/uploads/15e544ab7ec3e75b0d57b0b6ca2b9a3e/image.png)

- Xóa 1 image

`docker rmi IDimage`

- Check container đang chạy:

`docker container ls`
    
`docker ps`
    
thêm -a : list all container

- Xóa, pause/unpause container:

`docker container rm --force bb`
    
`docker pause/unpause ContainerID`
    

--force : khi container đang chạy , ‘bb’ là tên ref đến id container

- stop services :

`docker-compose stop`
    

- Xem các services đang chạy:

`docker-compose ps`

- Để tìm images có sẵn trong Docker Hub : 

`$ docker search ubuntu`

- Kiểm tra IP của container có 2 cách:

`$ docker inspect [container_name] | grep -i IP`

`$ docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id`

- Kiểm tra log của 1 container:

`$ docker logs –-tails 50 < container_id | container_name>`

- Liệt kê các volume mà container sử dụng (Volume trong docker là cơ chế lưu trữ dữ liệu sinh ra, được dùng để chia sẻ dữ liệu cho container)

`$ docker volume ls`

- Khi bạn khởi động image Docker, bạn có thể tạo, sửa đổi và xóa các tệp giống như bạn có thể với một máy ảo. 
 
-  Những thay đổi mà bạn thực hiện sẽ chỉ áp dụng cho container đó. Bạn có thể bắt đầu và dừng nó, nhưng một khi bạn hủy nó bằng lệnh docker rm, các thay đổi sẽ bị mất hoàn toàn.

- Tạo 1 image từ 1 container:

`$ docker commit -m "What you did to the image" -a "Author Name" [container_id][image_name]`

- Để xem lại lịch sử commit 

`$ docker history [image_name]`
  
  
-  Xem các thay đổi trên container: (Kiểm tra các thay đổi đối với tệ hoặc thư mục trên hệ thống của container)
 
 `$ docker diff [container_name]`


<a name="2.2"></a>
**2.2. Push, pull**

**-push :** share image lên repo ( ex: docker hub)

- step 1: create account in docker hub , create repo , $docker login in terminal

![image](/uploads/3777c490f3dfb83f83a9769f023aee1e/image.png)
  
- step 2: chỉnh sửa image sao cho nó có dạng “name_dockerhub/nameimage” với name_docekerhub trùng với tài khoản dockerhub (trường hợp này là genius98nd) , cú pháp sửa tên image:

`$docker image tag borabanei:1.0 genius98nd/borabanei:1.0`
- step 3: push image lên dockerhub

`$docker push genius98nd/borabanei:1.0`

**-Pull:**

`$docker pull genius98nd/borabanei:1.0`

<a name="III"></a> 
**III. Kiến thức thêm**

<a name="3.1"></a> 
**3.1.Docker CE/Docker EE**
> ref: [https://boxboat.com/2018/12/07/docker-ce-vs-docker-ee/](https://boxboat.com/2018/12/07/docker-ce-vs-docker-ee/)

- Docker CE (Docker Comunity Edittion) được phát hành năm 2013, là platform containerization mã nguồn mở miễn phí, có thể chạy trên nhiều hệ điều hành. Thường dành cho development, update mỗi tháng (với edge) và mỗi quý (với stable)

- Docker EE (Docker Enterprise) có thể nói là phiên bản trả phí premium của CE, released năm 2017 dành cho business-critical deployments (cung cấp dịch vụ cho các doanh nghiệp), update mỗi quý.

- Cả 2 đều được update định kì, tên phiên bản ( ex 17.03) theo định dạng năm.tháng update , và đều có sẵn trên những hdh phổ biến cũng như hạ tầng cloud

Docker EE cung cấp nhiều feature giúp doanh nghiệp lauch, quản lý và bảo mật:

- Gain access to certified Docker images and plugins

- View your container clusters in a single pane view

- Access controls for cluster and image management

- Run Docker EE engine with FIPS 140-2 certification

- Continuous vulnerability monitoring and Docker Security Scanning

Docker EE có 3 cấp:

- Docker Enterprise basic tier 
- Docker Enterprise standard tier
- Docker Enterprise advanced tier 
  
<a name="3.2"></a>
**3.2.Docker Engine**

> Docker Engine là ứng dụng client-server, như một công cụ để đóng gói ứng dụng, hỗ trợ công nghệ container để xử lý các nhiệm vụ và quy trình công việc liên quan đến việc xây dựng các ứng dụng dựa trên vùng chứa (container). với các thành phần chính.

![image](/uploads/60b7daf4f073631294882def7f3304d9/image.png)
  
![image](/uploads/b84d106275bcfee74081d716e43e09e0/image.png)
  

  
<a name="3.3"></a>
**3.3. Docker Compose**

>ref: [https://docs.docker.com/compose/](https://docs.docker.com/compose/)
Docker Compose là một công cụ giúp định nghĩa và chạy các ứng dụng Docker chạy trên nhiều containers thông qua các file cấu hình YAML. Thao tác sử dụng Compose thông qua ba bước:

B1: Định nghĩa môi trường ứng dụng Dockerfile cho tất cả các dịch vụ

B2: Tạo file docker-compose.yml định nghĩa tất cả các dịch vụ bên dưới ứng dụng

B3: Thực thi lệnh docker-compose up để chạy dịch vụ

*Các features:*

-   Multi isolated environments trên 1 host đơn
    
-   Toàn ven dữ liệu ở volume khi container được tạo (Compose tìm lại các volumes tương ứng của một containers ở lần khởi tạo trước đó và gán nó cho lần khởi tạo mới, đảm bảo dữ liệu cũ không bị mất đi)

    
-   Chỉ tạo lại container khi bị thay đổi (Khi restart 1 service mà ko bị thay đổi, compose sử dụng lại container hiện có.
    
-   Lưu trữ biến trong Compose files và tái sử dụng cho nhiều môi trường

**Các ca sử dụng phổ biến:**

- Triển khai môi trường
   
- Tự động hóa kiểm thử môi trường

- Triển khai trên Docker engine từ xa
    

Example: [https://gitlab.com/genius98nd/baocaotrain/-/tree/docker](https://gitlab.com/genius98nd/baocaotrain/-/tree/docker)

kết quả khi chạy các modify:

![image](/uploads/115e2179357e449cd4fd550226c83088/image.png)

+ Khi thêm volume, nó cho phép bạn thay đổi code ngay khi đang chạy mà không cần rebuild lại image

![image](/uploads/d97aff962141a2490e16333910517c2a/image.png)

  

<a name="3.4"></a>
**3.4. Docker instruction**
 [https://gitlab.com/genius98nd/baocaotrain/-/tree/appdockertrain](https://gitlab.com/genius98nd/baocaotrain/-/tree/appdockertrain)

**-FROM:** khai báo xem image sắp khởi tạo sẽ được xây dừng từ image gốc nào. Các image có thể lấy được từ Docker Hub.Command này phải được đặt trên cùng của Dockerfile

**-LABLE:** gán nhãn

` LABEL com.example.version="0.0.1-beta" com.example.release-date="2015-02-12"`
    

**-RUN:** RUN thực thi (các) lệnh trong một layer mới. Ví dụ: nó thường được sử dụng để cài đặt các gói phần mềm.

    RUN apt-get update && apt-get install -y \ 
    package-bar \
    
    package-baz \
    
    package-foo=1.3.*
    

**-CMD**: Sử dụng khi muốn thực thi (execute) một command trong quá trình build một container mới từ docker image, thường có cấu trúc : CMD ["executable", "param1", "param2"…]

` CMD ["apache2","-DFOREGROUND"]`

` CMD ["flask","run"]` Set the default command for the container to flask run

**-EXPOSE:** port của container nghe các kết nối

`  EXPOSE 80`
    

**-ENV:** Định nghĩa các biến môi trường

**-COPY, ADD:** cùng chức năng, copy được dùng nhiều hơn khi copy file cục bộ, khi nạp các gói từ xa ko nên dùng add mà nên thay bằng curl or wget. Copy một file từ host machine tới docker image. Có thể sử dụng URL cho tệp tin cần copy, khi đó docker sẽ tiến hành tải tệp tin đó đến thư mục đích.

`   COPY requirements.txt /tmp/`
    

**-ENTRYPOINT**: Định nghĩa những command mặc định, cái mà sẽ được chạy khi container running.

-**WORKDIR**: Định nghĩa working directory cho container khi được tạo 

-**VOLUME**: Cho phép truy cập / liên kết thư mục giữa các container và máy chủ (host machine)

`  VOLUME [/myvol]`
  

<a name="3.5"></a>
**3.5. Docker volume and bind mounts**

- Dùng để lưu trữ các dữ liệu độc lập với vòng đời của container giúp cho các containers có thể chia sẻ dữ liệu với nhau và với host.

- Giúp giữ lại dữ liệu khi một container bị xóa.

- Default tất cả những file tạo từ trong container sẽ được lưu trữ ở lớp container có thể ghi dữ liệu sẽ ko tồn tại nếu container đó khong tồn tại -> khó lấy ra nếu quá trình khác cần, khó di chuyển dữ liệu đi nơi khác 

- Có 2 option là dùng volume và bind mounts

**Volumn**


- Volumes được lưu trữ trong một phần filesystem của host do Docker quản lý (/var/lib/docker/volumes/ trong Linux).

- Volumes cũng support việc sử dụng volume drivers, cho phép lưu trữ dữ liệu trên máy chủ từ xa hoặc cloud providers.

- Chia sẻ data giữa multiple running containers. 

- Khi cần backup, restore, migrate data từ host này sang host khác thì volumes là lựa chọn tốt nhất.

**Bind mounts**

- Bind mounts có thể được lưu trữ ở bất kỳ đâu trên hệ thống máy chủ. Chúng có thể là các files or directories.

- Chia sẻ các files cấu hình từ máy chủ đến containers. Đây là cách Dockẻ cung cấp độ phân giải DNScho containers theo mặc định, bằng cách mount /etc/resolv.conf từ máy chủ đến mỗi containers.

- Chia sẻ source code giữa môi trường dev giữa host và containers. Ví dụ: có thể mount một thư mục Maven/ vào 1 container, và mỗi khi build project Maven trên Docker host, container sẽ có quyền truy cập để rebuild.

![image](/uploads/fd8189d33f93ef15bb1e9b67b96ff3d3/image.png)
    
+ Sử dụng volume để gắn (mount) một thư mục nào đó trong host với container.

`Docker run -it -v /_var_/data ubuntu`

→ gắn thư mục /_var_/data vào container ubuntu. Kiểm tra bằng việc tạo dữ liệu trên host và kiểm tra ở container


+ Sử dụng volume để chia sẽ dữ liệu giữa các container:

B1: Tạo container chứa volume

`docker create -v /linhlt --name volumecontainer ubuntu`

B2: Tạo container khác sử dụng container volumecontainer làm volume. Khi đó, mọi sự thay đổi trên container mới sẽ được cập nhật trong container volumecontainer:

`docker run -t -i --volumes-from volumecontainer ubuntu /bin/bash`


+ Sử dụng bind mount để chia sẻ dữ liệu giữa host và container 

Cụ thể là thư mục gắn trên máy host sẽ được mount với thư mục trên container, dữ liệu sinh ra trong thư mục được mount của container sẽ xuất hiện trong thư mục của host

B1: Tạo thư mục bindthis trên host (chứa container)

`$ mkdir bindthis`

B2: Thư mục /root/bindthis này sẽ được mount với thư mục /var/www/html/webapp nằm trên container.

`$ docker run -it -v $(pwd)/bindthis/:/var/www/html/webapp ubuntu bash`

B3: Tạo ra 1 file trong thư mục /var/www/html/webapp trên container.

`$ touch /var/www/html/webapp/index.html`

`$ exit`

B4: Kiểm tra xem trong thư mục /root/bindthis trên host có hay không.

`$ ls bindthis/`

Hien thi file **index.html**


**Backup và Restore volume**

- Backup thư mục volume là /trangtran trong container volumecontainer và nén lại dưới dạng file backup.tar.

`$ docker run --rm --volumes-from volumecontainer -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /trangtran`

- Restore: 

B1: Tạo volume /trangtran trên container data-container

`$ docker run -v /trangtran --name data-container ubuntu /bin/bash`

B2: Tạo container thực hiện nhiệm vụ giải nén file backup.tar vào thư mục /trangtran.

Container này có liên kết với container data-container ở trên.

`$ docker run --rm --volumes-from data-container -v $(pwd):/backup ubuntu bash -c "cd /trangtran && tar -zxvf /backup/backup.tar"`


<a name="3.6"></a>
**3.6. Docker network**

- Cung cấp private network (VLAN) để các container trên 1 host có thể liên lạc với nhau, hoặc các containers trên nhiều host có thể liên lạc với nhau (multi-host networking)

- Docker cung cấp tùy chọn để tạo và quản lý network riêng giữa các container.

**a) Use bridge networks**


Liệt kê Docker network

`$ docker network ls`

Tạo docker network

`$ docker network create -d bridge [bridge_network_name]`

Kết nối container với network bằng cách sử dụng tên container hoặc ID

`$ docker network connect [bridge_network_name][name/ID_container]`

Kiểm tra Docker network vừa kết nối:

`$ docker network inspect lan1`

Ngắt kết nối docker khỏi network

`$ docker network disconect lan1 [name/ID_container]`

Xóa docker network

`$ docker network rm lan1`

Xóa tất cả network không sử dụng khỏi system host:

`$ docker network prune`

`$docker network inspect bridge`  # xem chi tiết

- Mặc định khi chạy docker, default bridge network được tạo ra tự động. Bridge network được thể hiện bởi docker0 network. Trừ khi bạn chỉ định một option network khác bằng lệnh docker run --network=<NETWORK>, nếu ko Docker daemon sẽ tự động connect các containers tới loại network này, và các container chung mạng này sẽ giao tiếp được với nhau. Để giữa các container chạy trên các Docker daemon khác nhau, bạn cần quản lý định tuyển ở level OS or sử dụng overlay networks

- Bạn cũng có thể tự tạo ra user-defined networks, nó có nhiều điểm ưu việt hơn so với default:

-   Cung cấp cơ chế tự động DNS giữa 2 container, thay bằng phải sử dụng --link trong default
    
-   isolation tốt hơn vì ở mạng default nếu ko chỉ định --network thì container sẽ mặc định cho vào mạng này
    
-   Có thể gán và tách container ra khỏi vùng mạng nhanh chóng
    
-   Có thể config cho mỗi bridge riêng

- Bridge là driver tốt nhất cho việc giao tiếp multiple containers trên host đơn.

- Linhking ko được hỗ trợ trong đây, thay vào đó bạn có thể expose và pubish container ports trên các container trong networks này

![image](/uploads/1a85e51af29771b4623caca771b6cb1e/image.png)
  

**b) Overlay networks**

- Tạo ra 1 mạng distributed giữa các máy chủ docker , kết nối nhiều Docker daemons với nhau và cho phép các cụm services giao tiếp với nhau. Chúng ta có thể sử dụng overlay network để giao tiếp dễ dàng giữa cụm các services với một container độc lập, hay giữa 2 container với nhau ở khác máy chủ Docker daemons.

Và cho phép container kết nối đến (bao gồm cả các cụm containers) để giao tiếp một cách bảo mật. Docker đảm bảo định tuyến các gói tin đến và đi đúng container đích.

- Khi bạn tạo 1 swarm ott join host vào swarm thì sẽ có 2 mạng được tạo trên host đó:

-   1 mạng overlay : ingress-default nếu ko connect đến user-define overlay networks
    
-   1 mạng birdge : docker_gwbirdge , cái kết nối docker daemon riêng đến các docker daemon khác trong 1 swarm
    

- tạo overlay networks sử dụng với swarm services:

`docker network create -d overlay my-overlay`
    

- tạo overlay để được sử dụng bởi swarm services or giao tiếp giữa các container trên các docker daemon khác nhau

`$ docker network create -d overlay --attachable my-attachable-overlay`


<a name="IV"></a>
## IV. Issue: Link and net host in docker   

### Link : Giúp contanier giao tiếp với nhau thông qua tên service or alisas 
 **Khi nào cần sử dụng --Link:**
  >Sử dụng trong mạng default bridge. Mặc định khi không chỉ định 1 network bằng --network flag và drive network, thì container sẽ join vào mạng default bridge-các container trong mạng này có thể giao tiếp với nhau, nhưng chỉ qua IP address, trừ khi thêm --link flag để có thể giao tiếp qua tên service or alisa, link này chỉ là link 1 chiều.

  **Example** 
  -  Run 2 container từ image không dùng --network flag, sẽ mặc định join vào default bridge net 
 
![image](/uploads/85761dac68ba7d717e9857017da46ccf/image.png)

![image](/uploads/96cf9cc8a2a7ac948fdee7fe9ef75833/image.png)

>Docker engine sẽ tự set thông tin link vào /etc/hosts của wp container:

![image](/uploads/e1268163c88364586b221324c0d0495f/image.png)
> Vì Link là 1 chiều nên /etc/hosts trên redis container sẽ không có ánh xạ IP của wp

![image](/uploads/5878b8b0d910070b79763b67a9dd39c3/image.png)

*Khi bọn e run docker-compose, thì nó sẽ tự tạo 1 mạng user-define, với tên định dạng: namedir_default*
> Trong mạng user-define, các container có thể giao tiếp với nhau qua tên service or alisa, lưu tại file /etc/hosts của container, nên vì thế mà không cần dùng link trong trường hợp này.

- Dùng link khá bất tiện, phải định nghĩa rõ chiều kết nối, phải đảm bảo start container theo đúng thứ tự. Phương thức kết nối qua link đã bị xem là lịch sử. Hiện tại nó chỉ còn được dùng để đảm bảo thứ tự khởi chạy của các container.
- 
### Thử lại ví dụ này bằng việc chạy container ở net host


> Khi chạy ở mode net host, container’s network stack không bị cô lập khỏi Docker host , không có IP riêng mà nó sẽ sử dụng chung namespace net host, Published ports sẽ bị discard (ports: 8080:80 will be ignored)

![image](/uploads/984b96b70a554dc2cd320c850b71c856/image.png)

* Trong case này, cả phpmyadmin và wordpress đều mặc định port listen 80 (ko bind port được ở mode net host) , vì thế nó sẽ bị conflict. Khi docker-compose up, wordpress không khởi động được

![image](/uploads/681fd91a2af0187c4288dbb35745e976/image.png)
 
 > giải pháp bọn em đưa ra:

-  Sửa dockerfile -> với expose port listen của 2 container khác nhau ->> build lại image  
   kết quả: chưa thành công thì source code bọn e ko tìm được thông tin về expose port để sửa
- Vào container phpmyadmin, sửa port listen 2 file /etc/apache2/ports.conf và /etc/apache2/sites-enable/000-default.conf . vì mặc định apache sẽ lấy thông tin port listen từ 2 file đấy 
- /etc/apache2/ports.conf    

![image](/uploads/3df6d2226448eb1ea9f1214f0260d4cc/image.png)
- /etc/apache2/sites-enable/000-default.conf  

![image](/uploads/5ef9a1f1e6d55ddeb6c2729c17d9d85f/image.png)   

- `service apache2 restart`
- Note : Trước khi install vim để sửa file cần dùng lệnh "apt update", trường hợp fail có thể do chưa config proxy cho docker client .
- Config proxy cho docker client  : [https://docs.docker.com/network/proxy/](https://docs.docker.com/network/proxy/)

![image](/uploads/e0137161d3beb032f8616006f2b57f77/image.png)
 - phpmyadmin sẽ chạy được ở port 90, còn wp chạy port 80 

 ### NAT network trong docker

> Default, khi tạo 1 container, nó sẽ không publish cổng nào của nó ra bên ngoài, trừ khi sử  dụng `-p` flag. Lúc này sẽ tạo ra 1 firewall rule, map port của container đến 1 port trên docker host

> Để check  các thông tin về  bind port, em sử dụng `iptables`, tất cả Docker rule về bind port sẽ nằm ở  `chain Docker`

![image](/uploads/ba6ca58e0ac7f83a26bbb877e1a655a0/image.png)

*  `target : DNAT` (Destination Network Address Translation),ghi đè địa chỉ đích của gói tin.
*  `IN : !br-f20fbae` : áp dụng cho tất cả các kết nối đi vào trừ các kết nối trong mạng br-f20fbae
> các gói tin với  DesIP khác với IP trong vùng mạng br-f20fabe (vùng mạng của các container) sẽ được DNAT, cụ thể  nếu chúng có dạng IP:8080 sẽ được phân giải thành 172.20.0.2:80 - tương ứng với IP và expose port của phpmyadmin





#### 1. Cách iptables sinh ra bởi Docker trên hosts

- Mặc định, docker dùng bridge network. Khi cài đặt docker, sẽ thấy một interface docker0 trên host. Khi khởi chạy một container trong host, một interface mới đại diện cho nó sẽ được sinh ra trên host: `92: vethc4c9915@if91`

- Và trong container: `91: eth0@if92`. Interface `eth0@if92` có nghĩa nó sẽ map giữa interface 92 vethc4c9915@if91 đến eth0, gắn IP wan của docker host.

- Có thể xem ip của docker container qua:
```
[root@localhost ~]# docker inspect 992f2635e59f | grep IPAddr
           "SecondaryIPAddresses": null,
           "IPAddress": "172.17.0.2",
                   "IPAddress": "172.17.0.2",
```

- Lần lượt mỗi container được tạo ra sẽ dùng ip kế tiếp trong dải 172.17.0.0/16 Với 16 bist dành cho host thì số host có thể có tối đa là 2^16 - 2 container trong một host.
Tất cả các container đều nhận host làm gateway:
```
[root@992f2635e59f /]# route -n
Kernel IP routing table
Destination  Gateway   Genmask   Flags Metric Ref    Use Iface
0.0.0.0    172.17.0.1  0.0.0.0    UG    0      0     0   eth0
172.17.0.0   0.0.0.0  255.255.0.0   U   0      0     0   eth0
```
- Do các container đều dùng host làm gateway nên bridge network cho phép:

		+ các container trên cùng host giao tiếp với nhau được

		+ các container có thể gửi packet ra bên ngoài host được.

- Để đảm bảo host forward được IP thì nó cần được cấu hình NAT và cho phép ip forward.

Trên host cấu hình ip forward:
```
vi /etc/sysctl.conf
net.ipv4.conf.all.forwarding = 1
sysctl –p
```
- Mặc định, khi mới start service docker trên host, đã có những rule iptables do docker sinh ra. Những rule này là đủ để container có thể sử dụng host làm gateway để truy cập ra ngoài. Những rule này là based, không cần container run vẫn có.

- Ví dụ:
```
-A POSTROUTING -s 172.17.0.0/16 ! -o docker0 -j MASQUERADE
```
⇒ Thêm 1 rules vào POSTROUTING chain của bảng NAT. Tất cả các packet có source ip là 172.17.0.0/16 không đi đến interface docker0 sẽ được MASQUERADE nghĩa là package này sẽ được sửa đổi source IP sao cho giống out interface của host mà nó đi qua. (MASQUERADE phù hợp cho trường hợp external network, ở đây là network của host dynamically còn SNAT thì phải chỉ định chính xác IP đang được gán trên external network.)

```
-A FORWARD -o docker0 -j DOCKER
-A FORWARD -o docker0 -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
```
⇒ Hai rule này sẽ nằm trong FORWARD chain của filter table, tất cả các packet được forward đến docker0 sẽ được đẩy vào chain DOCKER, do chain DOCKER không khai báo gì nên nó ACCEPT, tiếp sau đó, với tất cả các packet được forward đến docker0 có trạng thái RELATED hoặc ESTABLISHED thì đều được ACCEPT.

- Chưa có DNAT nên từ ngoài truy cập vào dịch vụ trong container thì chưa được. DNAT được thực hiện ở PREROUTING, còn SNAT và MASQUERADE thực hiện ở POSTROUTING. Để từ bên ngoài truy cập dịch vụ bên trong container, cần phải expose service trong container. Thực chất là một kiểu port forwarding.
Để expose, khi run:
```
[root@localhost]# docker run -it -p 81:80 phpMyadmin
```
-> ở đây port trong container 80 sẽ được map đến 81 trong host.
- Việc map port forwarding này bản chất là một số chỉ dẫn được thêm vào iptables trong host:
```
-A DOCKER ! -i docker0 -p tcp -m tcp --dport 81 -j DNAT --to-destination 172.17.0.2:80
```

#### 2. Cách cấu hình insecure/live restore/rotate log của Docker

- Được cấu hình trong file : `/etc/docker/daemon.json`
- Default logging driver :  `json-file`

- Ví dụ:
```
{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "10m",
    "max-file": "3",
    "labels": "production_status",
    "env": "os,customer"
  }
}
```


